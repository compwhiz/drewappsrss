var app = new Vue({
    el: '#app',
    data: {
        message: 'Hello Vue!',
        showsplash: false,
        nextpage: false,
        results: '',
        summary: '',
        showing: false
    },
    mounted() {
        // this.showSplashScreen();
    },
    methods: {
        showNextScreen() {
            document.body.style.backgroundColor = "white";
            this.showsplash = false;
            this.nextpage = true;
            setTimeout(this.showRSS, 100);
        },
        showSplashScreen() {
            document.body.style.backgroundColor = "black";
            this.showsplash = true;
            setTimeout(this.showNextScreen, 100);
        },
        showRSS() {
            this.nextpage = false;
            document.body.style.backgroundColor = "white";
            window.addEventListener("keydown", this.handleKeydown);
            let Self = this;
            axios.get('https://newsapi.org/v2/top-headlines?country=us&apiKey=9243c2ed9ab341af9a32565843acdfb6')
                .then(function (response) {
                    // console.log(response.data);
                    Self.results = response.data.articles;
                }).catch(err => {
            });

        },
        showSomething(result) {
            this.showing = true;
            console.log(result);
            this.summary = result.content;
        },
        goDown(answer) {
            let Sel = this;
            this.showing = true;
            this.summary = answer;
        },
        handleKeydown(e) {
            if (e.key == "ArrowDown") {
                // console.log('down');
                $(".move:focus").next().focus();
            }
            if (e.key == "ArrowUp") {
                $(".move:focus").prev().focus();
            }
            if (!$(':focus').length) {
                // console.log("No focus set, now focusing back")
                $(".move")[0].focus();
            }
            if ((e.key == "Enter") && $(':focus').length) {
                this.goDown(document.activeElement.textContent);
                // console.log($(':focus span')[0]);
                // videoNameclick($(':focus')[0]);
            }
            if (e.key == "SoftRight") {
                alert("Nothing assigned to " + e.key);
            }
            if (e.key == "SoftLeft") {
                alert("Nothing assigned to " + e.key);
            }
            if (e.key == "Backspace") {
                // Handle if you want to go to previous menu or to exit directly;
                e.preventDefault(); // to prevent app from exiting
                let exit = confirm("Are your sure want to exit");
                if (exit) {
                    window.close();
                }
            }
        }

    }
});
