var path = require('path');
module.exports = {
    entry: './src/index.js',
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: "main.js",
        publicPath: "dist"
    },
    module: {
        rules: [
            {
                test: '/\.js$/',
                use: [{
                    loader: "babel-loader",
                    options: {
                        presets: ['es2015']
                    }
                }]
            },
        ]
    },
    devServer: {
        contentBase: path.join(__dirname, 'dist'),
        compress: true,
        port: 9000
    }
};